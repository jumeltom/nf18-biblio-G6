> Dans cette version de l'application la base de données a changé de structure !

# Ajout des contributeurs aux ressources

Pour simplifier la base de données nous avons ajouté les contributeur à l'aide du JSON directement dans la table
ressource.

## Strucuture *contributeurs*

Cette base de données regroupe les contributeurs dans la table des ressources.
Les auteurs ont pour structure :

```json
{
  "type": "contributeur",
  "nom": "Nom de l'auteur",
  "prenom": "Prénom de l'auteur",
  "dateNaissance": "Date de naissance de l'auteur",
  "nationalite": "Nationalité de l'auteur code ISO 3166-1 alpha-2",
  "role": "Enumération{acteur,realisateur,compositeur,interprete,auteur}"
}
```

Dans la table des ressources, on y retrouve un champs `auteurs` qui est un tableau d'auteurs.
exemple :

```json
[
  {
    "type": "contributeur",
    "nom": "Paul",
    "prenom": "Jean",
    "dateNaissance": "1970-01-01",
    "nationalite": "fr",
    "role": "acteur"
  },
  {
    "type": "contributeur",
    "nom": "Dupont",
    "prenom": "Jean",
    "dateNaissance": "1970-01-01",
    "nationalite": "fr",
    "role": "realisateur"
  }
]
```

### Strucuture de la table *ressources*

```sql
create table ressources
(
    code                varchar(10)  not null
        primary key,
    titre               varchar(255) not null,
    editeur             varchar(255) not null,
    genre               varchar(100) not null,
    code_classification varchar(10)  not null,
    contributeurs       json         not null default '[]'::json -- liste des contributeurs vide si null
);
```

### Migrer la base de données vers sa version RJSON

```sql
alter table ressources
    add column contributeurs json not null default '[]'::json;

drop table auteurs;
drop table filmcontributeurs;
drop table musiquecontributeurs;
drop table contributeurs;
```

# Ajout des données complémentaires pour les ressources

En plus des contributeurs nous avons aussi fusionné les information des livre, musique et film dans la table ressources.

## Strucutre de la ressource complémentaire

Grâce à la modularité du json nous avons pu ajouter des champs supplémentaires pour les ressources en fonction du type.
Fonction du type le JSON change

- Film
    ```json
    {
      "type": "film",
      "duree": "Durée du film en minutes",
      "synopsis": "Synopsis du film"
    }
    ```
- Musique
   ```json
   {
     "type": "musique",
     "duree": "Durée du film en minutes"
   }
   ```
- Livre
   ```json
   {
     "type": "livre",
     "isbn": "ISBN du livre",
     "resume": "Synopsis du livre",
     "langue": "Langue du livre code alpha-2 ISO 639-1"
   }
   ```

## Strucuture de la table *ressources*

```sql
create table ressources
(
    code                varchar(10)  not null
        primary key,
    titre               varchar(255) not null,
    editeur             varchar(255) not null,
    genre               varchar(100) not null,
    code_classification varchar(10)  not null,
    contributeurs       JSON_ARRAY         not null default '[]'::json, -- liste des contributeurs vide si null
    complement          json         not null default '{}'::json  -- données complémentaires vide si null
);
```

### Migrer la base de données vers sa version RJSON

```sql
alter table ressources
    add column complement json not null default '{}'::json;

-- supprime les views
drop view livres;
drop view films;
drop view musiques;
drop table films_infos;
drop table musiques_infos;
drop table livres_infos;
-- on recrée les views avec le json
create view livres as
select r.*
from ressources r
where r.complement ->> 'type' = 'livre';
create view films as
select r.*
from ressources r
where r.complement ->> 'type' = 'film';
create view musiques as
select r.*
from ressources r
where r.complement ->> 'type' = 'musique';
```

# Exemple d'utilisation
```sql
insert into Ressources VALUES('TEST20','Mcdo le film','UTC','Comedie','999','[{"type": "contributeur",
    "nom": "Paul",
    "prenom": "Jean",
    "dateNaissance": "1970-01-01",
    "nationalite": "fr",
    "role": "acteur"
  },
  {
    "type": "contributeur",
    "nom": "Dupont",
    "prenom": "Jean",
    "dateNaissance": "1970-01-01",
    "nationalite": "fr",
    "role": "realisateur"
  }]
', '{
  "type": "film",
  "duree": 900,
  "synopsis": "Un film sur McDo"
}
');



INSERT INTO Ressources 
VALUES('B17L','l agent secret',' Maison d édition Gallimard',
'roman d aventures',825,
'{  "type": "contributeur",
    "nom": "Clancy",
    "prenom": "Robert",
    "dateNaissance": "1986-04-08",
    "nationalite": "fr",
    "role": "auteur"
 }',
'{
  "type": "livre",
  "isbn": "8746508374",
  "resume": "un espion au sein de l état",
  "langue": "FR"
}'
);



SELECT *
FROM Ressources R, JSON_ARRAY_ELEMENTS(R.contributeurs) contrib
WHERE contrib->>"nom" = "Dupont"

SELECT *
FROM Ressources R
WHERE R.complement->>"type"="film"

```
![imxsx.png](./imxsx.png)
