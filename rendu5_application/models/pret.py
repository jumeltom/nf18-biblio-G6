""""
{
    "id_pret": 2,
    "exemplaire": 0,
    "adherent": 1,
    "date_debut": "2022-12-15",
    "etat": "waiting",
    "duree": 2
  }
"""
import datetime
from termcolor import colored


class Pret:
    def __init__(self, id_pret, exemplaire, adherent, date_debut, etat, duree):
        self.id_pret = id_pret
        self.exemplaire = exemplaire
        self.adherent = adherent
        self.date_debut = date_debut
        self.etat = etat
        self.duree = duree

    def __str__(self):
        return f"ID: {self.id_pret}, Exemplaire: {self.exemplaire}, Adherent: {self.adherent}, Date de debut: {self.date_debut}, Etat: {self.etat}, Duree: {self.duree}"

    def __repr__(self):
        return f"ID: {self.id_pret}, Exemplaire: {self.exemplaire}, Adherent: {self.adherent}, Date de debut: {self.date_debut}, Etat: {self.etat}, Duree: {self.duree}"

    def __eq__(self, other):
        return self.id_pret == other.id_pret

    def isClosed(self):
        return self.etat == "done"

    def isWaiting(self):
        return self.etat == "waiting"

    def isLate(self):
        return self.isWaiting() and self.date_debut + datetime.timedelta(days=self.duree) < datetime.date.today()

    def getLateDays(self):
        return datetime.date.today() - (self.date_debut + datetime.timedelta(days=self.duree))

    def pretty_print(self, show_id=True, show_state=True):
        print("Pret :")
        if show_id:
            print("\tID: ", self.id_pret)
        print("\tExemplaire: ", self.exemplaire)
        print("\tAdherent: ", self.adherent)
        print("\tDate de debut: ", self.date_debut)
        if show_state:
            print("\tEtat: ", colored(self.pretty_state(), "red" if self.isLate() else "green"))
        print("\tDuree: ", self.duree)

    def pretty_state(self):
        if self.isClosed():
            return "Fini !"
        elif self.isLate():
            return "En retard"
        elif self.isWaiting():
            return "En cours"
