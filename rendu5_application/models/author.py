"""
{
    "id": 1,
    "nom": "Hugo",
    "prenom": "Victor",
    "date_naissance": "1802-02-26",
    "nationalite": "FR"
  }
"""
import pycountry


class Author:
    def __init__(self, id, nom, prenom, date_naissance, nationalite):
        self.id = id
        self.nom = nom
        self.prenom = prenom
        self.date_naissance = date_naissance
        self.nationalite = nationalite

    def pretty_print(self):
        print("Nom: ", self.nom)
        print("Prenom: ", self.prenom)
        print("Date de naissance: ", self.date_naissance)
        countries = {}
        for country in pycountry.countries:
            countries[country.alpha_2] = country.name
        if self.nationalite in countries:
            print("Nationalité: ", countries[self.nationalite])
        else:
            print("Nationalite: ", self.nationalite)
