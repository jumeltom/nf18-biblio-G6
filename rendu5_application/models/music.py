"""
  {
    "code": "MT88",
    "titre": "Marche Turc",
    "editeur": "Erwan Records",
    "genre": "classique",
    "code_classification": "89M",
    "code_ressource": "MT88",
    "duree": 400
  }
"""


class Music:
    def __init__(self, code, titre, editeur, genre, code_classification, code_ressource, duree):
        self.code = code
        self.titre = titre
        self.editeur = editeur
        self.genre = genre
        self.code_classification = code_classification
        self.code_ressource = code_ressource
        self.duree = duree

    def __str__(self):
        return "Code: %s\nTitre: %s\nEditeur: %s\nGenre: %s\nCode classification: %s\nCode ressource: %s\nDuree: %s" % (
            self.code, self.titre, self.editeur, self.genre, self.code_classification, self.code_ressource, self.duree)

    def __repr__(self):
        return "Code: %s\nTitre: %s\nEditeur: %s\nGenre: %s\nCode classification: %s\nCode ressource: %s\nDuree: %s" % (
            self.code, self.titre, self.editeur, self.genre, self.code_classification, self.code_ressource, self.duree)

    def pretty_print(self):
        print("Code: %s\nTitre: %s\nEditeur: %s\nGenre: %s\nCode classification: %s\nCode ressource: %s\nDuree: %s" % (
            self.code, self.titre, self.editeur, self.genre, self.code_classification, self.code_ressource, self.duree))
