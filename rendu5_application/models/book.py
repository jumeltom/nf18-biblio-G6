"""
  {
    "code": "A12E",
    "titre": "Les Miserables",
    "editeur": "Ldp Jeunesse",
    "genre": "fiction",
    "code_classification": "53",
    "code_ressource": "A12E",
    "isbn": "2010008995",
    "resume": "Un classique de literrature francaise",
    "langue": "FR"
  }
"""


class Book:
    def __init__(self, code, titre, editeur, genre, code_classification, code_ressource, isbn, resume, langue):
        self.code = code
        self.titre = titre
        self.editeur = editeur
        self.genre = genre
        self.code_classification = code_classification
        self.code_ressource = code_ressource
        self.isbn = isbn
        self.resume = resume
        self.langue = langue

    def __str__(self):
        return "Code: %s\nTitre: %s\nEditeur: %s\nGenre: %s\nCode classification: %s\nCode ressource: %s\nISBN: %s\nResume: %s\nLangue: %s" % (
            self.code, self.titre, self.editeur, self.genre, self.code_classification, self.code_ressource, self.isbn,
            self.resume, self.langue)

    def __repr__(self):
        return "Code: %s\nTitre: %s\nEditeur: %s\nGenre: %s\nCode classification: %s\nCode ressource: %s\nISBN: %s\nResume: %s\nLangue: %s" % (
            self.code, self.titre, self.editeur, self.genre, self.code_classification, self.code_ressource, self.isbn,
            self.resume, self.langue)

    def pretty_print(self):
        print("Code: %s\nTitre: %s\nEditeur: %s\nGenre: %s\nCode classification: %s\nCode ressource: %s\nISBN: %s\nResume: %s\nLangue: %s" % (
            self.code, self.titre, self.editeur, self.genre, self.code_classification, self.code_ressource, self.isbn,
            self.resume, self.langue))
