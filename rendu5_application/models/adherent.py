class Adherent:
    def __init__(self, user, birthday, phone, card):
        self.user = user
        self.birthday = birthday
        self.phone = phone
        self.card = card

    def __repr__(self):
        return "<Adherent('%s','%s','%s','%s')>" % (self.user, self.birthday, self.phone, self.card)

    def __str__(self):
        return "Adherent('%s','%s','%s','%s')" % (self.user, self.birthday, self.phone, self.card)

    def pretty_print(self):
        print("Adherent :")
        print("\tAnniversaire: ", self.birthday)
        print("\tTéléphone: ", self.phone)
        print("\tBadge: ", self.card)
