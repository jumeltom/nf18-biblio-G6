"""
{
    "code": "A12E",
    "titre": "Les Miserables",
    "editeur": "Ldp Jeunesse",
    "genre": "fiction",
    "code_classification": "53"
  }
"""
from termcolor import colored


class Ressource:
    def __init__(self, code, titre, editeur, genre, code_classification):
        self.code = code
        self.titre = titre
        self.editeur = editeur
        self.genre = genre
        self.code_classification = code_classification

    def __str__(self):
        return "Code: %s\nTitre: %s\nEditeur: %s\nGenre: %s\nCode classification: %s" % (
            self.code, self.titre, self.editeur, self.genre, self.code_classification)

    def __repr__(self):
        return "Titre: %s\nCode: %s\nEditeur: %s\nGenre: %s\nCode classification: %s" % (
            self.titre, self.code, self.editeur, self.genre, self.code_classification)

    def pretty_print(self):
        print("%s: %s\n%s: %s\nEditeur: %s\nGenre: %s\nCode classification: %s" % (
            colored("Titre", "green"), self.titre, colored("Code", "green"), self.code, self.editeur, self.genre,
            self.code_classification))
