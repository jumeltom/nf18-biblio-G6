class User:
    def __init__(self, userid, nom, prenom, addresse, mail):
        self.userId = userid
        self.nom = nom
        self.prenom = prenom
        self.addresse = addresse
        self.mail = mail

    def __repr__(self):
        return "<User('%s','%s','%s','%s','%s')>" % (self.userId, self.nom, self.prenom, self.addresse, self.mail)

    def __str__(self):
        return "User('%s','%s','%s','%s','%s')" % (self.userId, self.nom, self.prenom, self.addresse, self.mail)

    def __eq__(self, other):
        return self.userId == other.userId

    def pretty_print(self):
        print("Utilisateur :")
        print("\tNom: ", self.nom)
        print("\tPrenom: ", self.prenom)
        print("\tAddresse: ", self.addresse)
        print("\tMail: ", self.mail)
