from abc import abstractmethod, ABC


class DefaultProblem(ABC):
    def __init__(self, id):
        self.id = id

    @abstractmethod
    def pretty_print(self):
        return

    @abstractmethod
    def is_solved(self):
        return False
