"""
 {
    "code": "TT09",
    "titre": "Titanic",
    "editeur": "Hollywood",
    "genre": "Drame",
    "code_classification": "FT67",
    "code_ressource": "TT09",
    "duree": 200,
    "synopsis": "Un bateau frappe un iceberg"
  }
"""
from termcolor import colored


class Film:
    def __init__(self, code, titre, editeur, genre, code_classification, code_ressource, duree, synopsis):
        self.code = code
        self.titre = titre
        self.editeur = editeur
        self.genre = genre
        self.code_classification = code_classification
        self.code_ressource = code_ressource
        self.duree = duree
        self.synopsis = synopsis

    def __str__(self):
        return "Code: %s\nTitre: %s\nEditeur: %s\nGenre: %s\nCode classification: %s\nCode ressource: %s\nDuree: %s\nSynopsis: %s" % (
            self.code, self.titre, self.editeur, self.genre, self.code_classification, self.code_ressource, self.duree,
            self.synopsis)

    def __repr__(self):
        return "Titre: %s\nCode: %s\nEditeur: %s\nGenre: %s\nCode classification: %s\nCode ressource: %s\nDuree: %s\nSynopsis: %s" % (
            self.titre, self.code, self.editeur, self.genre, self.code_classification, self.code_ressource, self.duree,
            self.synopsis)

    def pretty_print(self):
        print("%s: %s\n%s: %s\nEditeur: %s\nGenre: %s\nCode classification: %s\nDuree: %s min\nSynopsis: %s" % (
            colored("Titre", "green"), self.titre, colored("Code", "green"), self.code, self.editeur, self.genre,
            self.code_ressource, self.duree, self.synopsis))
