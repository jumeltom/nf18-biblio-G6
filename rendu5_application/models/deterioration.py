"""
{
    "id_pret":0,
    "exemplaire":"d",
    "etat":"done",
}
"""
from models.default_problem import DefaultProblem


class Deterioration(DefaultProblem):
    def __init__(self, id_pret, exemplaire, etat, ):
        super(Deterioration, self).__init__(id_pret)
        self.exemplaire = exemplaire
        self.etat = etat

    def pretty_print(self):
        print("Id du prêt : #", self.id)
        print("Etat : ", self.etat)
        print("Régularisé" if self.is_solved() else "A Régulariser !")

    def is_solved(self):
        return self.etat == 'done'
