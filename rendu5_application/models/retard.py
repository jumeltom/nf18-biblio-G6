""""
    {
    "id_pret": 2,
    "exemplaire": "d",
    "date_retour": "2022-12-14",
    "duree_retard": 3
  }
"""
import datetime

from models.default_problem import DefaultProblem


class Retard(DefaultProblem):
    def __init__(self, id_pret, exemplaire, date_retour, duree_retard):
        super().__init__(id_pret)
        self.exemplaire = exemplaire
        self.date_retour = date_retour
        self.duree_retard = duree_retard

    def pretty_print(self):
        print("Id du prêt :1 #", self.id)
        print("Date te retour : ", self.date_retour)
        print("Duree du retard : ", self.duree_retard)
        print("Régularisé" if self.is_solved() else "A Régulariser !")

    def is_solved(self):
        return self.date_retour + datetime.timedelta(days=self.duree_retard) < datetime.date.today()
