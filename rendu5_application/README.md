# 📖 Application python pour le projet bibliothèque

## Description

Ce projet est une application python qui permet de gérer une bibliothèque. Il permet de gérer les livres, les auteurs, les emprunts et les lecteurs...

## 💻 Installation

### Prérequis

- Python 3.7
- pip
- virtualenv (facultatif)

### Installation

- Cloner le projet
- Installer les dépendances
```bash
cd application
pip install -r requirements.txt
```
- Changez les variables d'environnement dans le fichier **.env** pour concorder avec la configuration de votre base de données

## 🚀 Lancement

- Lancer l'applicaiton
```bash
python .\main.py
```
 - Puis créez-vous un compte :)
