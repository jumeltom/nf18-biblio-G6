from services.connector import database
from services.ressource_repo import RessourceRepository


def search_menu_handler(choice):
    ressource_repo = RessourceRepository(database)
    if choice == 1:
        search = input("Entrez votre recherche : ")
        if search == "":
            print("Recherche invalide")
            return
        ressources = ressource_repo.search_for_ressource(search)
        if len(ressources) == 0:
            print("Aucun résultat")
        else:
            for res in ressources:
                print("----------------")
                res.pretty_print()
        input("Entrez pour continuer")

    elif choice == 2:
        books = ressource_repo.get_books()
        if len(books) == 0:
            print("Aucun livre")
        else:
            for book in books:
                print("----------------")
                book.pretty_print()
        input("Entrez pour continuer")
    elif choice == 3:
        films = ressource_repo.get_films()
        if len(films) == 0:
            print("Aucun film")
        else:
            for film in films:
                print("----------------")
                film.pretty_print()
        print("----------------")
        input("Entrez pour continuer")
    elif choice == 4:
        musics = ressource_repo.get_musics()
        if len(musics) == 0:
            print("Aucune musique")
        else:
            for music in musics:
                print("----------------")
                music.pretty_print()
        print("----------------")
        input("Entrez pour continuer")
    elif choice == 5:
        code = input("Entrez le code de la ressource : ")
        if code == "":
            print("Code invalide")
            return
        authors = ressource_repo.get_authors_of(code)
        if authors is None:
            print("Aucun auteur")
        else:
            if len(authors) == 0:
                print("Aucun auteur")
            else:
                for author in authors:
                    print("----------------")
                    author.pretty_print()
        print("----------------")
        input("Entrez pour continuer")
