from services.services import *


def add_ressource_menu_handler(choice):
    ressource_repo = RessourceRepository(database)

    if choice == 1:
        nom = input("Donner le nom")
        prenom = input("Donner le prenom")
        date_naissance = input('Donner la date de naissance')
        nationalite = input('Donner la nationalité')
        code = input('Donner le code de son oeuvre')

        ressource_repo.add_contributor(nom, prenom, date_naissance, nationalite)
        print("----------------")
        input("Entrez pour continuer")

    elif choice == 2:
        idContrib = input("Donner l'identifiant du contributeur ")
        code = input("Donner le code de la ressource")

        ressource_repo.add_auteur(idContrib, code)
        print("----------------")
        input("Entrez pour continuer")
    elif choice == 3:

        idContrib = input("Donner l'identifiant du contributeur ")
        code = input("Donner le code de la ressource")
        role = input("Donner le role")

        ressource_repo.add_musique_contributeur(idContrib, code, role)

        print("----------------")
        input("Entrez pour continuer")
    elif choice == 4:

        idContrib = input("Donner l'identifiant du contributeur ")
        code = input("Donner le code de la ressource")
        role = input("Donner le role")

        ressource_repo.add_film_contributeur(idContrib, code, role)

        print("----------------")
        input("Entrez pour continuer")

    elif choice == 5:
        choix_ressource = input(
            "Quelle type de ressource voulez-vous ajouter:\n\t1-Livre \n\t2-Musique \n\t3-Film\nChoix : ")

        if choix_ressource in range(1, 4):
            code = input("Donner le code")
            titre = input("Donner le titre")
            editeur = input("Donner l'editeur")
            genre = input("Donner le genre")
            code_classification = input("Donner le code de classification")

            ressource_repo.add_ressource(code, titre, editeur, genre, code_classification)

            if choix_ressource == 1:
                isbn = input("Donner l'isbn")
                resume = input("Donner le resume")
                langue = input("Donner la langue")

                ressource_repo.add_book(code, isbn, resume, langue)

            if choix_ressource == 2:
                duree = input("Donner la duree du film")
                synopsis = input("Donner le synopsis")

                ressource_repo.add_film(code, duree, synopsis)

            if choix_ressource == 3:
                duree = input("Donner la duree de la musique")

                ressource_repo.add_music(code, duree)
        print("----------------")
        input("Entrez pour continuer")

    elif choice == 6:
        id_exemplaire = input("Donner l'id de l'exemplaire")
        ressource = input("Donner le code de la ressource")
        etat = input("Donner l'etat : ('neuf','bon','correct','abime','tres_abime')")
        try:
            ressource_repo.add_exemplaire(id_exemplaire, ressource, etat)
        except Exception:
            print("Une erreur est survenue")
        print("----------------")
        input("Entrez pour continuer")
