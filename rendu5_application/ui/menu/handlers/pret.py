import datetime

from services.services import pretRepo, account, ressourceRepo


def prets_menu_handler(choice):
    if choice == 1:
        if not pretRepo.can_make_pret(account.user_id):
            print("Vous ne pouvez pas emprunter pour l'instant à cause de vos retard de prets")
            input("Appuyez sur entrée pour continuer")
            return
        code = input("Entrez le code de la ressource : ")
        exemplaires = ressourceRepo.get_availables_exemplaires_of(code)
        if len(exemplaires) == 0:
            print("Aucun exemplaire disponible")
        else:
            print("%d exemplaires disponibles !" % (len(exemplaires)))
            duree = input("Entrez la durée de votre réservation (en jours): ")
            pretRepo.add_pret(account.user_id, exemplaires[0], duree)
            print("Réservation effectuée, vous pouvez récuperer votre exemplaire.")
        input("Appuyez sur entrée pour continuer")
    elif choice == 2:
        res = pretRepo.get_all_waiting(account.user_id)
        if len(res) == 0:
            print("Aucun prêt en attente")
        else:
            for pret in res:
                pret.pretty_print()
        input("Entrez pour continuer")
    elif choice == 3:
        # history
        res = pretRepo.get_all_done_for_adh(account.user_id)
        if len(res) == 0:
            print("Aucun prêt")
        else:
            for pret in res:
                pret.pretty_print()
        input("Entrez pour continuer")
    elif choice == 4:
        # accidents
        res = pretRepo.get_all_accidents(account.user_id)
        if len(res) == 0:
            print("Aucun accident")
        else:
            for item in res:
                print("----------------------")
                item.pretty_print()
        print("----------------------")
        input("Entrez pour continuer")


def manage_pret_menu_handler(choice):
    if choice == 1:
        adh_id = input("Entrez l'identifiant de l'adhérent : ")
        res = pretRepo.get_all_waiting(adh_id)
        if len(res) == 0:
            print("Aucun prêt en attente")
        else:
            for pret in res:
                pret.pretty_print()
            id = input("Entrez l'id du prêt à fermer : ")
            pret = None
            for item in res:
                if item.id_pret == int(id):
                    pret = item
                    break
            if pret is None:
                print("Pret non trouvé")
                input("Appuyez sur entrée pour continuer")
                return
            ressource = pretRepo.get_exemplaire_of_pret(id)
            state = input(
                "Rappel des etats : \n\t('neuf','bon','correct','abime','tres_abime')\nEntrez le nouvel état de la ressource si il a changé (%s) : " % (
                    ressource[2],))
            if state != "":
                # le nouvel état est différent de l'ancien état
                # il faut donc le mettre à jour et ajouter un accident
                ressourceRepo.update_state(ressource[0], state)
                # on demande rempboursement si l'état est mauvais, on le note dans tous les cas
                pretRepo.add_deterioration(id, state, "waiting" if state == "tres_abime" else "done")
            # check le retard
            if pret.isLate():
                days = pret.getLateDays()
                # on ajoute l'accident
                pretRepo.add_retard(pret.id_pret, datetime.date.today(), days)
            # enfin on peut fermer le pret
            pretRepo.close_pret(pret.id_pret)
        input("Entrez pour continuer")
    if choice == 2:
        adh_id = input("Entrez l'identifiant de l'adhérent : ")
        res = pretRepo.get_all_done_for_adh(adh_id)
        [item.pretty_print() for item in res]
        res = pretRepo.get_all_waiting(adh_id)
        [item.pretty_print() for item in res]
        print("------------")
        input("Entrer pour continuer")
