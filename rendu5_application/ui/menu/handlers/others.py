from termcolor import colored

from services.services import *
from ui.menu.handlers.add_ressource import add_ressource_menu_handler
from ui.menu.handlers.pret import prets_menu_handler, manage_pret_menu_handler
from ui.menu.handlers.search import search_menu_handler
from ui.menu.handlers.user_rights import account_menu_handler
from ui.menu.menu import draw_menu
from ui.menu.menus_def import *


def main_connected_handler(choice):
    if choice == 1:
        draw_menu(search_menu, "Recherche", search_menu_handler, "Retour")
    elif choice == 2:
        user = userRepo.get_user(account.user_id)
        adh = userRepo.get_adherent(user)
        if adh is None and not userRepo.is_personal_member(user):
            print(colored("Devenez adhérent !", "green"))
            print(colored("Pour devenir adhérent, vous devez payer une cotisation de 10€", "blue"))
            print(colored("Addressez-vous à un membre de la bibliothèque pour devenir adhérent", "blue"))
            input("Entrez pour continuer")
        else:
            if userRepo.is_personal_member(user):
                draw_menu(add_ressource_menu, "Gestion des ressources", add_ressource_menu_handler, "Retour")
            else:
                draw_menu(prets_menu, "Prets", prets_menu_handler, "Retour")
    elif choice == 3:
        draw_menu(account_menu, "Compte", account_menu_handler, "Retour")
    elif choice == 4:
        user = userRepo.get_user(account.user_id)
        if userRepo.is_personal_member(user):
            draw_menu(manage_prets_menu, "Gestion des prêts", manage_pret_menu_handler, "Retour")