import os
from services.services import *


def admin_sudo_menu_handler(choice):
    if choice == 1:
        print("Entrez le mot de passe sudo")
        password = input("Mot de passe : ")
        if password == os.getenv("SUDO_PASSWORD"):
            username = input("Entrez le nom d'utilisateur : ")
            if username == "":
                print("Nom d'utilisateur invalide")
                return
            # check if username exist
            user = userRepo.get_user_by_username(username)
            if user is None:
                print("Nom d'utilisateur invalide")
            else:
                if not userRepo.is_personal_member(user):
                    userRepo.set_as_personal_member(user)
                    print("Utilisateur %s est maintenant membre du personnel" % username)
        input("Entrez pour continuer")
    elif choice == 2:
        print("Entrez le mot de passe sudo")
        password = input("Mot de passe : ")
        if password == os.getenv("SUDO_PASSWORD"):
            username = input("Entrez le nom d'utilisateur : ")
            if username == "":
                print("Nom d'utilisateur invalide")
                return
            # check if username exist
            user = userRepo.get_user_by_username(username)
            if user is None:
                print("Nom d'utilisateur invalide")
            else:
                if userRepo.is_personal_member(username):
                    userRepo.set_as_not_personal_member(user)
                    print("Utilisateur %s n'est plus membre du personnel" % username)
        input("Entrez pour continuer")
    elif choice == 3:
        for user in userRepo.get_all_personal_members():
            print("----------------")
            user.pretty_print()
        input("Entrez pour continuer")


def account_menu_handler(choice):
    if choice == 1:
        """Voir mes informations"""
        user = userRepo.get_user(account.user_id)
        adh = userRepo.get_adherent(user)
        user.pretty_print()
        if adh is None:
            print("Vous n'êtes pas adhérent")
        else:
            adh.pretty_print()
        input("Entrez pour continuer")
    elif choice == 2:
        mdp = input("Entrez votre mot de passe actuel : ")
        if mdp == "":
            print("Mot de passe invalide")
            input("Entrez pour continuer")
            return
        isgood = UserAccounts(database).is_good_password(account.user_id, mdp)
        if isgood:
            newmdp = input("Entrez votre nouveau mot de passe : ")
            if newmdp == "":
                print("Mot de passe invalide")
                return
            UserAccounts(database).change_password(account.user_id, newmdp)
            print("Mot de passe changé")
            input("Entrez pour continuer")
        else:
            print("Mot de passe incorrect")
            input("Entrez pour continuer")