import os
from termcolor import colored


def draw_menu(items, title, handler, quit_sentence="Quitter"):
    error = None
    while True:
        os.system("clear")
        print(colored(title, "green"))
        print('----------------')
        for i, item in enumerate(items):
            print(colored(str(i + 1), "blue") + '. ' + item)
        print('----------------')
        print(colored("0", "blue") + '. %s' % (quit_sentence,))
        print('----------------')
        if error is not None:
            print(colored(error, 'red'))
            error = None
        choice = input('Entrez votre choix: ')
        if choice == '0':
            return
        try:
            choice = int(choice)
            if 0 < choice <= len(items):
                try:
                    res = handler(choice)
                    if res is True:
                        return
                except Exception as e:
                    error = "Une erreur est survenue durant le traitement de votre choix : %s" % str(e)
            else:
                error = 'Choix invalide'
        except ValueError:
            error = "Veuillez entrer un nombre ex:\n\t - '1' pour %s" % (items[0],)
            pass
