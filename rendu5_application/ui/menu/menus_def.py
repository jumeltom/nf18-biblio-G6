main_menu = [
    "Se connecter",
    "S'inscrire",
]

main_connected_menu = [
    "Rechercher",
    "Prêts",
    "Mon compte",
]

search_menu = [
    "Rechercher par titre",
    "Lister les livres",
    "Lister les films",
    "Lister les musiques",
    "Obtenir les détails d'une ressource",
]

add_ressource_menu = [
    "Ajouter un contributeur",
    "Ajouter un auteur",
    "Ajouter un contributeur de musique",
    "Ajouter un contributeur de film",
    "Ajouter une ressource",
    "Ajouter un exemplaire"
]

prets_menu = [
    "Faire un prêt",
    "Lister les prêts en cours",
    "Historique des prêts",
    "Lister les accidents",
]
manage_prets_menu = [
    "Fermer un prêt",
    "Lister les prêts pour une carte d'adhérent",
]

account_menu = [
    "Voir mes informations",
    "Modifier mon mot de passe"
]

admin_sudo_menu = [
    "Ajouter un membre du personnel",
    "Supprimer un membre du personnel",
    "Lister les membres du personnel",
]
