import os
import psycopg2


class Connector:
    def __init__(self, host, port, user, password, database):
        self.bdd = None
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.database = database

    def connect(self):
        self.bdd = psycopg2.connect("host=localhost dbname=nf18 user=postgres password=admin port=5432")
        return self.bdd

    def close(self):
        self.bdd.close()
        return True

    def commit(self):
        self.bdd.commit()
        return True

    def cursor(self):
        if self.bdd is not None:
            return self.bdd.cursor()
        else:
            return self.connect().cursor()


database = Connector(host=os.getenv("DB_HOST"), port=os.getenv("DB_PORT"), user=os.getenv("DB_USER"), password=os.getenv("DB_PASS"), database=os.getenv("DB_NAME1"))
