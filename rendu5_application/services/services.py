from services.connector import database
from services.pret_repository import PretRepository
from services.ressource_repo import RessourceRepository
from services.user_connector import UserAccounts
from services.user_repository import UserRepository

userRepo = UserRepository(database)
account = UserAccounts(database)
pretRepo = PretRepository(database)
ressourceRepo = RessourceRepository(database)

