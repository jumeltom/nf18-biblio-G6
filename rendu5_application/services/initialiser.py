# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 16:33:27 2022

@author: Erwan
"""
from connector import *


def initialiser_db(conn):
    cur = conn.cursor()

    file = open("init/database.sql", "r")
    sql = file.read()
    cur.execute(sql)
    database.commit()
    file.close()

    file = open("init/insert.sql", "r")
    sql = file.read()
    cur.execute(sql)
    database.commit()
    file.close()


initialiser_db(database)
