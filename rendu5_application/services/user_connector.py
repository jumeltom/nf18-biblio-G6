import os

import bcrypt

from services.user_repository import _user_from_db_row


class UserAccounts:
    def __init__(self, db):
        self.db = db
        self.user_id = None

    def connect_user(self, username, password):
        # pre-check if user is sudo from .env
        if username == os.getenv("ADMIN_USER") and password == os.getenv("ADMIN_PASS"):
            return -1
        curr = self.db.cursor()
        curr.execute("SELECT id_membre,password FROM compteutilisateur WHERE login = %s", (username,))
        if curr.rowcount == 0:
            return None
        row = curr.fetchone()
        user_id = row[0]
        self.user_id = user_id
        hashed = row[1]
        curr.close()
        try:
            if bcrypt.checkpw(password.encode('utf-8'), hashed.encode('utf-8')):
                return user_id
            else:
                return None
        except ValueError:
            return None

    def user_exists(self, username):
        curr = self.db.cursor()
        curr.execute("SELECT * FROM compteutilisateur WHERE login = %s", (username,))
        res = curr.rowcount != 0
        curr.close()
        return res

    def create_user(self, user, username, password):
        curr = self.db.cursor()
        curr.execute("INSERT INTO compteutilisateur (id_membre,login, password) VALUES (%s,%s, %s)",
                     (user.userId, username, bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')))
        self.db.commit()
        curr.close()
        return True

    def is_good_password(self, user_id, mdp):
        curr = self.db.cursor()
        curr.execute("SELECT password FROM compteutilisateur WHERE id_membre = %s", (user_id,))
        if curr.rowcount == 0:
            return False
        row = curr.fetchone()
        hashed = row[0]
        curr.close()
        try:
            if bcrypt.checkpw(mdp.encode('utf-8'), hashed.encode('utf-8')):
                return True
            else:
                return False
        except ValueError:
            return False

    def change_password(self, user_id, newmdp):
        curr = self.db.cursor()
        curr.execute("UPDATE compteutilisateur SET password = %s WHERE id_membre = %s",
                     (bcrypt.hashpw(newmdp.encode('utf-8'), bcrypt.gensalt()).decode('utf-8'), user_id))
        self.db.commit()
        curr.close()
        return True
