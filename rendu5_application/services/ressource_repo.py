from models.author import Author
from models.book import Book
from models.film import Film
from models.music import Music
from models.ressource import Ressource


def _ressource_from_db(param):
    return Ressource(
        param[0],
        param[1],
        param[2],
        param[3],
        param[4],
    )


def _book_from_db_row(database_row):
    return Book(
        database_row[0],
        database_row[1],
        database_row[2],
        database_row[3],
        database_row[4],
        database_row[5],
        database_row[6],
        database_row[7],
        database_row[8],
    )


def _film_from_db_row(database_row):
    return Film(
        database_row[0],
        database_row[1],
        database_row[2],
        database_row[3],
        database_row[4],
        database_row[5],
        database_row[6],
        database_row[7],
    )


def _music_from_db_row(database_row):
    return Music(
        database_row[0],
        database_row[1],
        database_row[2],
        database_row[3],
        database_row[4],
        database_row[5],
        database_row[6],
    )


def _author_from_db(row):
    return Author(
        row[0],
        row[1],
        row[2],
        row[3],
        row[4],
    )


class RessourceRepository:
    def __init__(self, db):
        self.db = db

    def get_ressource(self, book_id):
        curr = self.db.cursor()
        curr.execute("SELECT * FROM ressources WHERE code = %s", (book_id,))
        if curr.rowcount == 0:
            return None
        res = _ressource_from_db(curr.fetchone())
        curr.close()
        return res

    def search_for_ressource(self, book_title):
        curr = self.db.cursor()
        book_title = '%' + book_title + '%'
        curr.execute("SELECT * FROM ressources WHERE titre LIKE %s", (book_title,))
        res = [_ressource_from_db(row) for row in curr.fetchall()]
        curr.close()
        return res

    def get_books(self):
        curr = self.db.cursor()
        curr.execute("SELECT * FROM livres")
        res = [_book_from_db_row(row) for row in curr.fetchall()]
        curr.close()
        return res

    def get_book(self, book_id):
        curr = self.db.cursor()
        curr.execute("SELECT * FROM livres WHERE code = %s", (book_id,))
        if curr.rowcount == 0:
            return None
        res = _book_from_db_row(curr.fetchone())
        curr.close()
        return res

    def get_film(self, film_id):
        curr = self.db.cursor()
        curr.execute("SELECT * FROM films WHERE code = %s", (film_id,))
        if curr.rowcount == 0:
            return None
        res = _book_from_db_row(curr.fetchone())
        curr.close()
        return res

    def get_films(self):
        curr = self.db.cursor()
        curr.execute("SELECT * FROM films")
        res = [_film_from_db_row(row) for row in curr.fetchall()]
        curr.close()
        return res

    def get_musics(self):
        curr = self.db.cursor()
        curr.execute("SELECT * FROM musiques")
        res = [_music_from_db_row(row) for row in curr.fetchall()]
        print(curr.fetchall());
        curr.close()
        return res

    def get_music(self, music_id):
        curr = self.db.cursor()
        curr.execute("SELECT * FROM musiques WHERE code = %s", (music_id,))
        if curr.rowcount == 0:
            return None
        res = _music_from_db_row(curr.fetchone())
        curr.close()
        return res

    def get_authors_of(self, code):
        curr = self.db.cursor()
        curr.execute(
            "SELECT c.* FROM auteurs a join contributeurs c on c.id = a.id_contributeur  WHERE a.code_ressource = %s",
            (code,))
        if curr.rowcount == 0:
            return None
        res = [_author_from_db(row) for row in curr.fetchall()]
        curr.close()
        return res

    def get_availables_exemplaires_of(self, code):
        curr = self.db.cursor()
        curr.execute(
            "select id from exemplaire where ressource = %s and id not in (select exemplaire from pret where pret.etat != 'done')",
            (code,))
        res = [row[0] for row in curr.fetchall()]
        curr.close()
        return res

    def add_ressource(self, code, titre, editeur, genre, code_classification):
        curr = self.db.cursor()
        curr.execute("INSERT INTO ressources (code,titre,editeur,genre,code_classification) VALUES (%s,%s, %s,%s,%s)",
                     (code, titre, editeur, genre, code_classification))
        self.db.commit()
        curr.close()
        return True

    def add_book(self, code_ressource, isbn, resume, langue):
        curr = self.db.cursor()
        curr.execute("INSERT INTO Livres_infos (code_ressource,isbn,resume,langue) VALUES (%s,%s, %s,%s)",
                     (code_ressource, isbn, resume, langue))
        self.db.commit()
        curr.close()
        return True

    def add_film(self, code_ressource, duree, synopsis):
        curr = self.db.cursor()
        curr.execute("INSERT INTO Films_infos (code_ressource,duree,synopsis) VALUES (%s,%s, %s,%s)",
                     (code_ressource, duree, synopsis))
        self.db.commit()
        curr.close()
        return True

    def add_music(self, code_ressource, duree):
        curr = self.db.cursor()
        curr.execute("INSERT INTO Musiques_infos (code_ressource,duree) VALUES (%s,%s, %s)",
                     (code_ressource, duree))
        self.db.commit()
        curr.close()
        return True

    def add_contributor(self, nom, prenom, date_naissance, nationalite):
        curr = self.db.cursor()
        curr.execute("INSERT INTO Contributeurs(nom,prenom,date_naissance,nationalite) VALUES (%s,%s, %s,%s)",
                     (nom, prenom, date_naissance, nationalite))

        self.db.commit()
        curr.close()
        return True

    def add_auteur(self, idContrib, code):
        curr = self.db.cursor()
        curr.execute("INSERT INTO Auteurs(id_contributeur,code_ressource) VALUES(%s,%s)", (idContrib, code))

        self.db.commit()
        curr.close()
        return True

    def add_film_contributeur(self, idContrib, code, role):
        curr = self.db.cursor()
        curr.execute("INSERT INTO FilmContributeurs(id_contributeur,code_ressource,role) VALUES(%s,%s,%s)",
                     (idContrib, code, role))

        self.db.commit()
        curr.close()
        return True

    def add_musique_contributeur(self, idContrib, code, role):
        curr = self.db.cursor()
        curr.execute("INSERT INTO MusiqueContributeurs(id_contributeur,code_ressource,role) VALUES(%s,%s,%s)",
                     (idContrib, code, role))

        self.db.commit()
        curr.close()
        return True

    def add_exemplaire(self, idExemplaire, ressource, etat):
        curr = self.db.cursor()
        curr.execute("INSERT INTO Exemplaire(id,ressource,etat) VALUES (%s,%s, %s)",
                     (idExemplaire, ressource, etat))
        self.db.commit()
        curr.close()
        return True

    def update_state(self, idExemplaire, etat):
        curr = self.db.cursor()
        curr.execute("UPDATE exemplaire SET etat = %s WHERE id = %s", (etat, idExemplaire))
        self.db.commit()
        curr.close()
        return True