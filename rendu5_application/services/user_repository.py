from models.adherent import Adherent
from models.user import User


def _user_from_db_row(param):
    return User(
        param[0],
        param[1],
        param[2],
        param[3],
        param[4],
    )


def _adherent_from_db_row(user, database_row):
    return Adherent(
        user,
        database_row[1],
        database_row[2],
        database_row[3],
    )


class UserRepository:
    def __init__(self, db):
        self.db = db

    def get_user(self, user_id):
        curr = self.db.cursor()
        curr.execute("SELECT * FROM membres WHERE id = %s", (user_id,))
        if curr.rowcount == 0:
            return None
        res = _user_from_db_row(curr.fetchone())
        curr.close()
        return res

    def get_users(self):
        curr = self.db.cursor()
        curr.execute("SELECT * FROM membres")
        res = [_user_from_db_row(row) for row in curr.fetchall()]
        curr.close()
        return res

    def create_user(self, user):
        curr = self.db.cursor()
        curr.execute("INSERT INTO membres (nom, prenom, addresse, email) VALUES (%s, %s, %s, %s) RETURNING id",
                     (user.nom, user.prenom, user.addresse, user.mail))
        self.db.commit()
        user.userId = curr.fetchone()[0]
        curr.close()
        return user

    def update_user(self, user):
        curr = self.db.cursor()
        curr.execute("UPDATE membres SET nom = %s, prenom = %s, addresse = %s, email = %s WHERE id = %s",
                     (user.nom, user.prenom, user.addresse, user.mail, user.userId))
        self.db.commit()
        curr.close()
        return True

    def delete_user(self, user_id):
        curr = self.db.cursor()
        curr.execute("DELETE FROM membres WHERE id = %s", (user_id,))
        self.db.commit()
        curr.close()
        return True

    def get_adherent(self, user):
        curr = self.db.cursor()
        curr.execute("SELECT * FROM adherents WHERE id_membre = %s", (user.userId,))
        if curr.rowcount == 0:
            return None
        res = _adherent_from_db_row(user, curr.fetchone())
        curr.close()
        return res

    def is_personal_member(self, user):
        curr = self.db.cursor()
        curr.execute("select * from membredupersonnel where id_membre = %s", (user.userId,))
        return curr.rowcount != 0

    def set_as_personal_member(self, user):
        curr = self.db.cursor()
        curr.execute("insert into membredupersonnel (id_membre) values (%s)", (user.userId,))
        self.db.commit()
        curr.close()
        return True

    def get_user_by_username(self, username):
        curr = self.db.cursor()
        curr.execute(
            "SELECT m.* FROM compteutilisateur inner join membres m on m.id = compteutilisateur.id_membre WHERE compteutilisateur.login = %s",
            (username,))
        if curr.rowcount == 0:
            return None
        res = _user_from_db_row(curr.fetchone())
        curr.close()
        return res

    def set_as_not_personal_member(self, user):
        curr = self.db.cursor()
        curr.execute("delete from membredupersonnel where id_membre = %s", (user.userId,))
        self.db.commit()
        curr.close()
        return True

    def get_all_personal_members(self):
        curr = self.db.cursor()
        curr.execute("select * from membredupersonnel as mp inner join membres as m on m.id = mp.id_membre")
        res = [self.get_user(row[0]) for row in curr.fetchall()]
        curr.close()
        return res
