from typing import List

from models.default_problem import DefaultProblem
from models.deterioration import Deterioration
from models.pret import Pret
from models.retard import Retard


def _pret_from_db_row(database_row):
    return Pret(
        database_row[0],
        database_row[1],
        database_row[2],
        database_row[3],
        database_row[4],
        database_row[5],
    )


class PretRepository:
    def __init__(self, db):
        self.db = db

    def get_all_done_for_adh(self, adherent_id):
        curr = self.db.cursor()
        curr.execute("SELECT * FROM pret WHERE adherent  = %s AND etat='done' order by date_debut desc ",
                     (adherent_id,))
        res = [_pret_from_db_row(row) for row in curr.fetchall()]
        curr.close()
        return res

    def get_all_waiting(self, adherent_id):
        curr = self.db.cursor()
        curr.execute("SELECT * FROM pret WHERE etat = 'waiting' AND adherent = %s", (adherent_id,))
        res = [_pret_from_db_row(row) for row in curr.fetchall()]
        curr.close()
        return res

    def get_all_closed(self):
        curr = self.db.cursor()
        curr.execute("SELECT * FROM pret WHERE etat = 'closed'")
        res = [_pret_from_db_row(row) for row in curr.fetchall()]
        curr.close()
        return res

    def get_all_late(self):
        curr = self.db.cursor()
        curr.execute("SELECT * FROM pret WHERE etat = 'waiting' AND (date_debut+duree) < NOW()")
        res = [_pret_from_db_row(row) for row in curr.fetchall()]
        curr.close()
        return res

    def get_all_accidents(self, adherent_id):
        return self.get_all_deteriorations(adherent_id) + self.get_all_retards(adherent_id)

    def get_all_deteriorations(self, adherent_id) -> list[DefaultProblem]:
        curr = self.db.cursor()
        curr.execute(
            "SELECT p.id_pret,p.exemplaire,d.etat_remboursement FROM pret p join deterioration d on p.id_pret = d.id_pret where adherent=%s",
            (adherent_id,))
        res = [Deterioration(row[0], row[1], row[2]) for row in curr.fetchall()]
        curr.close()
        return res

    def get_all_retards(self, adherent_id) -> list[DefaultProblem]:
        curr = self.db.cursor()
        curr.execute(
            "SELECT p.id_pret,p.exemplaire,d.date_retour,d.duree_retard FROM pret p join retard d on p.id_pret = d.id_pret where adherent=%s",
            (adherent_id,))
        res = [Retard(row[0], row[1], row[2], row[3], ) for row in curr.fetchall()]
        curr.close()
        return res

    def can_make_pret(self, adh):
        for d in (self.get_all_retards(adh)):
            if not d.is_solved():
                return False
        return True

    def add_pret(self, user_id, exemplaire_id, duree):
        curr = self.db.cursor()
        curr.execute(
            "insert into pret (exemplaire,adherent, date_debut,duree,etat) values (%s,%s,current_date,%s,'waiting')",
            (exemplaire_id, user_id, duree,))
        self.db.commit()
        curr.close()

    def add_retard(self, id_pret, date_retour, duree_retard):
        curr = self.db.cursor()
        curr.execute("INSERT INTO Retard(id_pret,date_retour,duree_retard) VALUES (%s,%s, %s)",
                     (id_pret, date_retour, duree_retard))
        self.db.commit()
        curr.close()
        return True

    def add_deterioration(self, id_pret, etat_remboursement):
        curr = self.db.cursor()
        curr.execute("INSERT INTO deterioration (id_pret, etat_remboursement) VALUES (%s,%s)",
                     (id_pret, etat_remboursement))
        self.db.commit()
        curr.close()
        return True

    def get_exemplaire_of_pret(self, id):
        curr = self.db.cursor()
        curr.execute(
            "SELECT id,ressource,exemplaire.etat FROM exemplaire inner join pret p on p.exemplaire = exemplaire.id where p.id_pret = %s",
            (id,))
        if curr.rowcount == 0:
            return None
        res = curr.fetchone()
        curr.close()
        return res

    def close_pret(self, id_pret):
        curr = self.db.cursor()
        curr.execute("UPDATE pret SET etat='done' where id_pret=%s", (id_pret,))
        self.db.commit()
        curr.close()
        return True
