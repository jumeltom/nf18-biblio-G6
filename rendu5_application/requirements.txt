termcolor~=2.1.0
pycountry~=22.3.5
psycopg2~=2.9.5
bcrypt~=4.0.1
python-dotenv~=0.21.0