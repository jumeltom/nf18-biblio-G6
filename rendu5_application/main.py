import os

from models.user import User
from services.services import *
from ui.menu.handlers.others import main_connected_handler
from ui.menu.handlers.user_rights import admin_sudo_menu_handler
from ui.menu.menu import draw_menu
from ui.menu.menus_def import *
from dotenv import load_dotenv

load_dotenv()


def _draw_connect():
    in_username = input("Entrez votre nom d'utilisateur : ")
    while in_username == "":
        in_username = input("Entrez votre nom d'utilisateur : ")
    quit_flag = False
    while quit_flag is False:
        in_password = input("Entrez votre mot de passe pour %s : " % in_username)
        while in_password == "":
            in_password = input("Entrez votre mot de passe : ")
        if in_password == "q":
            break
        user_id = account.connect_user(in_username, in_password)
        if user_id is not None and user_id != -1:
            return userRepo.get_user(user_id)
        else:
            if user_id == -1:
                draw_menu(admin_sudo_menu, "Administration sudo", admin_sudo_menu_handler, "Retour")
                return None
            else:
                print("Nom d'utilisateur ou mot de passe incorrect q pour quitter")


def main_handler(choice):
    if choice == 1:
        connect_res = _draw_connect()
        os.system('clear')
        if connect_res is not None:
            adh = userRepo.get_adherent(connect_res)
            if adh is None:
                main_connected_menu[1] = "Devenir adhérent"
            else:
                main_connected_menu[1] = "Prêts"
            if userRepo.is_personal_member(connect_res):
                main_connected_menu[1] = "Gérer les ressources"
                main_connected_menu.append("Gérer les prêts")
            draw_menu(main_connected_menu, "Bonjour %s %s" % (connect_res.nom, connect_res.prenom),
                      main_connected_handler, "Se déconnecter")
        else:
            print("Vous n'êtes pas connecté")
    elif choice == 2:
        os.system('clear')
        print("Creation de compte")
        nom = input("Entrez votre nom : ")
        if nom == "":
            print("Nom invalide")
            return
        prenom = input("Entrez votre prénom : ")
        if prenom == "":
            print("Prénom invalide")
            return
        addresse = input("Entrez votre addresse : ")
        if addresse == "":
            print("Addresse invalide")
            return
        mail = input("Entrez votre mail : ")
        if mail == "":
            print("Mail invalide")
            return
        user = userRepo.create_user(User(0, nom, prenom, addresse, mail))
        if user is not None:
            print("Compte créé !")
            print("Création de votre compte utilisateur")
            username = input("Choisissez votre nom d'utilisateur : ")
            # check if username is already taken
            while username == "" or account.user_exists(username):
                print("Nom d'utilisateur déjà pris")
                username = input("Choisissez votre nom d'utilisateur : ")
            password = input("Entrez votre mot de passe : ")
            if password == "":
                print("Mot de passe invalide")
                return
            account.create_user(user, username, password)
            print("Vous êtes inscrit !")


os.system("color")
draw_menu(main_menu, "Menu principal", main_handler)
