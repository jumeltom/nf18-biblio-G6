# Modele logique de données

## Partie 1:
Choix de la transformation d'heritage:

> On a un type d'heritage non complet avec une classe mère abstraite(Membres),

> de plus la classe mére comporte d'autres associations, ce qui nous suggére une tranformation par référence.
 
> on considère que "Adhérent" et "MembreDuPersonnel" sont deux systèmes,donc on considère que l'héritage est exclusif.

> Contraite:
 Projection(Membre,id)=( Projection(Adherent,id_membre) UNION Projection(MembreDuPersonnel,id_membre) )
 INTERSECTION(PROJECTION(Adherent,id_membre),PROJECTION(MembreDuPersonnel,id_membre))={} (héritage exclusif)


 
* **Membre**(#id_membre:INTEGER , nom:VARCHAR(10), prénom:VARCHAR(10), adresse:VARCHAR(15), email:VARCHAR(15) )

* **Adhérent**(#id_membre => Membre , date_de_naissance :DATE , numéro_de_téléphone :VARCHAR(15) , carte_adhérent:
  VARCHAR(
    20) ) avec carte_adhérent UNIQUE


* **MembreDuPersonnel**(#id_membre => Membre  )


* **CompteUtilisateur**(#login: VARCHAR(10), mdp: VRARCHAR(10), id_membre => Membre  ) avec id-membre UNIQUE

## Partie 2

Un pret peut etre fait seulement après la vérification que l'adhérent n'a pas plus que 5 sanctions.

* **Pret**(#id_pret: INTEGER , date_debut :DATE , etat:{en cours , etat_fini} , durée: INTEGER , exemplaire =>
  Exemplaire.id,
  adherent =>Adherent.id_membre) avec adhérent NOT NULL et exemplaire NOT NULL

La contraint pour l'association entre "prêt" et "Adhérent" est "adhérent NOT NULL" (le cardinalité est 1-0..N)

La contraint pour l'association entre "prêt" et "Exemplaire" est "exemplaire NOT NULL" (le cardinalité est 1-0..N)



Dans cette partie de transformation de l'héritage,nous avons changés le MCD et donnés la nouvelle version qui est appelée "Projet3" dans gitlab.
parce que la classe mère qui est vide est abstrait et il n'y a pas d'autre association sur classe mère,et il existe d'autre association sur classes filles(héritage non complet),donc nous choisissons une tranformation par les classes filles.

* **Retard**(#id_pret => Pret.id_pret , date_retour: DATE , Durée_retard :INTEGER ,id_pret => Pret.id_pret)

* **Deterioration**(#id_pret => Pret.id_pret , etat_remboursement : {etat_remboursement_en attente ,
  etat_remboursement_fait},id_pret => Pret.id_pret)

* **Exemplaire**(#id : integer , ressource =>Ressource.code, etat : {etat_neuf , etat_bon , etat_correct , etat_abimé ,
  etat_tres_abimé})


## Partie 3

* **Contributeur**(#id : integer, nom: VARCHAR, prenom: VARCHAR, data_de_naissance : date, nationalité : VARCHAR)


Un héritage par référence a été choisi parce que Ressource est directement réferéncé par 1...* Examplaires. Ne sachant pas si Examplaire référence un Livre, un Film ou une Musique, on décide de garder la table Ressource en tant que classe abstraite.
L'héritage par classe mère est une possibilité intéressante. Elle est mis à l'écart en raison de l'abstraction de la classe mère et des complictions survenants avec les associations Ressource-Contributeur en fonction du type de ressource.  

Il faut ajouter la contrainte que tous les tuples de Ressource sont référencés par un tuple de Livre ou Film ou Musique.

PROJECTION(Ressource,code)=PROJECTION(Livre,ressource) UNION PROJECTION(Film,ressource) UNION PROJECTION(Musique,ressource) (classe mère abstrait)
INTERSECTION(PROJECTION(Livre,ressource),PROJECTION(Film,ressource),PROJECTION(Musique,ressource))={} (héritage exclusif)

* **Ressource**(#code : VARCHAR, titre: VARCHAR,editeur: VARCHAR, genre: VARCHAR, code_de_classification : VARCHAR)


* **Livre**(#ressource =>Ressource.code, ISBN: VARCHAR, resumé : VARCHAR, lang_doc : VARCHAR )
    * **Auteur**(#contrib =>Contributeur.id, #livre =>Livre.ressource)


* **Film**(#ressource =>Ressource.code, synopsis : VARCHAR, longueur: integer, ) 

    *   **FilmRole** enum('Realisteur', 'Acteur') 
    * **FilmContributeur**(#contrib =>Contributeur.id, #film => Film.ressource, role : FilmRole) avec role not null
  



* **Musique**(#ressource =>Ressource.code, longueur: integer)

    * **MusiqueRole** enum('Compositeur', 'Interprète') 

     * **MusiqueContributeur**(#contrib =>Contributeur.id, #musique => Musique.ressource, role : MusiqueRole) avec role not null

   
