-- test env delete all
delete
from retard
where true;
delete
from deterioration
where true;
delete
from pret
where true;
delete
from adherents
where true;
delete
from membredupersonnel
where true;
delete
from compteutilisateur
where true;
delete
from membres
where true;
delete
from exemplaire
where true;
delete
from ressources
where true;
delete
from musiques_infos
where true;
delete
from Contributeurs
where true;
delete
from MusiqueContributeurs
where true;
delete
from Films_infos
where true;
delete
from FilmContributeurs
where true;

-- creation de ressources
-----------Les Miserables------------------------
INSERT INTO Ressources
VALUES ('A12E',
        'Les Miserables',
        'Ldp Jeunesse',
        'fiction',
        '53');
INSERT INTO Livres_infos
VALUES ('A12E',
        '2010008995',
        'Un classique de literrature francaise',
        'FR');

INSERT INTO Contributeurs
VALUES (1,
        'Hugo',
        'Victor',
        '1802-02-26',
        'FR');

INSERT INTO Auteurs
VALUES (1,
        'A12E');

-------------------Marche Turc---------------------------------d

INSERT INTO Ressources
VALUES ('MT88',
        'Marche Turc',
        'Erwan Records',
        'classique',
        '89M');
INSERT INTO musiques_infos
VALUES ('MT88',
        '400');
INSERT INTO Contributeurs
VALUES (0,
        'Mozart',
        'Wolfgang Amadeus',
        '1756-01-27',
        'AT');

INSERT INTO MusiqueContributeurs
VALUES (0,
        'MT88',
        'compositeur');


---------------Titanic---------------------
INSERT INTO Ressources
VALUES ('TT09',
        'Titanic',
        'Hollywood',
        'Drame',
        'FT67');

INSERT INTO Films_infos
VALUES ('TT09',
        200,
        'Un bateau frappe un iceberg');

INSERT INTO Contributeurs
VALUES (3,
        'Cameron',
        'James',
        '1954-08-16',
        'US');
INSERT INTO FilmContributeurs
VALUES (3,
        'TT09',
        'realisateur');

-- creation de membres
INSERT INTO Membres
VALUES (36,
        'Bovet',
        'Jean',
        '05 route de Garges,95200',
        'Jbovet@hotmail.fr');
INSERT INTO Membres
VALUES (20,
        'Bovet',
        'Edouard',
        '02 route de Garges,95200',
        'Edoura@biblio-informatique.fr');

INSERT INTO Adherents
VALUES (36,
        '2000-07-03',
        '+33651491236',
        'FA563');

INSERT INTO MembreDuPersonnel
VALUES (20);

INSERT INTO CompteUtilisateur
VALUES (36,
        'mathieuMon',
        'Math091998/');


-- creation de pret
INSERT INTO Exemplaire
VALUES (1,
        'A12E',
        'neuf');
INSERT INTO Pret
VALUES (0, 1, 36, '2000-07-03', 'done',
        1);
-- creation de retard et détérioration
INSERT INTO Retard
VALUES (0,
        '2000-07-06',
        '3');
INSERT INTO Deterioration
VALUES (0,
        'waiting');
