-- drop already existing database tables
drop table if exists Membres cascade;
drop table if exists Adherents cascade;
drop table if exists MembreDuPersonnel cascade;
drop table if exists CompteUtilisateur cascade;
drop table if exists Contributeurs cascade;
drop table if exists Ressources cascade;
drop table if exists Livres_infos cascade;
drop table if exists Films_infos cascade;
drop table if exists Musiques_infos cascade;
drop table if exists Pret cascade;
drop table if exists Retard cascade;
drop table if exists Deterioration cascade;
drop table if exists Exemplaire cascade;
drop table if exists Auteurs cascade;
drop table if exists FilmContributeurs cascade;
drop table if exists MusiqueContributeurs cascade;

-- drop already exist datatypes
drop type if exists etat_exemplaire cascade ;
drop type if exists film_contributeur_type cascade ;
drop type if exists musique_contributeur_type cascade ;
drop type if exists etat_remboursement_type cascade ;
drop type if exists etat_pret cascade ;

-- drop already exist views
drop view if exists Livres;
drop view if exists Films;
drop view if exists Musiques;

-- #Partie 1: Gestion des utilisateurs
create table Membres
(
    id       serial primary key, -- serial est un alias d'auto_increment pour postgreSQL
    nom      varchar(50)  not null,
    prenom   varchar(25)  not null,
    addresse varchar(255) not null,
    email    varchar(255) not null
);
create table Adherents
(
    id_membre int unique,
    birthday  date,
    phone     varchar(15),
    card      varchar(50) unique,
    foreign key (id_membre) references Membres (id)
);
create table MembreDuPersonnel
(
    id_membre int unique,
    foreign key (id_membre) references Membres (id)
);
create table CompteUtilisateur
(
    id_membre int unique,
    login     varchar(10) unique,
    password  varchar(150) not null,
    foreign key (id_membre) references Membres (id)
);
-- # Partie 2 prestations de la bibliothèque
--      Ressources
create table Ressources
(
    code                varchar(10) primary key,
    titre               varchar(255) not null,
    editeur             varchar(255) not null,
    genre               varchar(100) not null,
    code_classification varchar(10)  not null
);
CREATE TYPE etat_exemplaire AS ENUM ('neuf','bon','correct','abime','tres_abime');
create table Exemplaire
(
    id        integer primary key,
    ressource varchar(10),
    etat      etat_exemplaire NOT NULL,
    foreign key (ressource) references Ressources (code)
);

create table Livres_infos
(
    code_ressource varchar(10) primary key,
    isbn           varchar(13) not null,
    resume         varchar(1000),
    langue         char(2)     not null, -- Code langue ISO 639-1
    foreign key (code_ressource) references Ressources (code) on delete cascade
);
create table Films_infos
(
    code_ressource varchar(10) primary key,
    duree          int not null,
    synopsis       varchar(1000),
    foreign key (code_ressource) references Ressources (code) on delete cascade
);

create table Musiques_infos
(
    code_ressource varchar(10) primary key,
    duree          int not null,
    foreign key (code_ressource) references Ressources (code) on delete cascade
);
-- Views pour les infos complémentaires des ressources simplifiant les requêtes
create view Livres as
select *
from Ressources
         inner join Livres_infos on Ressources.code = Livres_infos.code_ressource;

create view Films as
select *
from Ressources
         inner join Films_infos on Ressources.code = Films_infos.code_ressource;

create view Musiques as
select *
from Ressources
         inner join Musiques_infos on Ressources.code = Musiques_infos.code_ressource;
-- # Partie 3 contenue de la bibliothèque
create table Contributeurs
(
    id             serial primary key,
    nom            varchar(50) not null,
    prenom         varchar(25) not null,
    date_naissance date        not null,
    nationalite    char(2)     not null -- Code pays ISO 3166-1 alpha-2
);
-- sous types de contributeurs
create table Auteurs
(
    id_contributeur int,
    code_ressource  varchar(10),
    primary key (id_contributeur, code_ressource),
    foreign key (id_contributeur) references Contributeurs (id) on delete cascade,
    foreign key (code_ressource) references Ressources (code) on delete cascade
);
CREATE TYPE film_contributeur_type AS ENUM ('acteur', 'realisateur');
create table FilmContributeurs
(
    id_contributeur int,
    code_ressource  varchar(10),
    role            film_contributeur_type not null,
    primary key (id_contributeur, code_ressource),
    foreign key (id_contributeur) references Contributeurs (id) on delete cascade,
    foreign key (code_ressource) references Ressources (code) on delete cascade
);
CREATE TYPE musique_contributeur_type AS ENUM ('compositeur', 'interprete');
create table MusiqueContributeurs
(
    id_contributeur int,
    code_ressource  varchar(10),
    role            musique_contributeur_type not null,
    primary key (id_contributeur, code_ressource),
    foreign key (id_contributeur) references Contributeurs (id) on delete cascade,
    foreign key (code_ressource) references Ressources (code) on delete cascade
);
-- # Partie 4 gestion des prêts
CREATE TYPE etat_pret AS ENUM ('waiting','done');
-- contrainte , un exemplaire ne peut être prêté qu'une seule fois à la fois
create table Pret
(
    id_pret    serial primary key,
    exemplaire int       not null,
    adherent   int       not null,
    date_debut date,
    etat       etat_pret NOT NULL,
    duree      integer,
    foreign key (exemplaire) references Exemplaire (id),
    foreign key (adherent) references Adherents(id_membre)
);
create table Retard
(
    id_pret      int primary key,
    date_retour  date,
    duree_retard integer,
    foreign key (id_pret) references Pret (id_pret)
);
CREATE TYPE etat_remboursement_type AS ENUM ('waiting','done');
create table Deterioration
(
    id_pret            int primary key,
    etat_remboursement etat_remboursement_type NOT NULL,
    foreign key (id_pret) references Pret (id_pret)
);