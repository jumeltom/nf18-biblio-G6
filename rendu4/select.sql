
-----Affichage des informations de l'ensemble des Adhèrents de la Bibliothèque------------------------
SELECT card,nom,prenom,phone,email,addresse
FROM MEMBRES M JOIN ADHERENTS A
ON A.id_membre = M.id

----Affichage de l'ensemble des memebres du personnel de la bibliothèque-----------------------

SELECT id,nom,prenom,email
FROM MEMBRES M JOIN MEMBREDUPERSONNEL MP
ON M.id = MP.id_membre

-----Les Adherents qui ont emprunté des ressources (sans aucun retard)---------------------------
SELECT card,nom,prenom,id_pret,exemplaire,genre,code_classification,date_debut
FROM PRET P JOIN ADHERENTS A
ON P.adherent = A.id_membre
JOIN Membres M
ON M.id=A.id_membre
JOIN exemplaire E 
ON E.id = P.exemplaire
JOIN RESSOURCES R
ON R.code=E.ressource
WHERE P.etat ='done'

----Les Adherents qui ont des retards de 'retour de pret'---------------------
SELECT card,nom,prenom,id_pret,exemplaire,genre,code_classification,date_debut
FROM PRET P JOIN ADHERENTS A
ON P.adherent = A.id_membre
JOIN Membres M
ON M.id=A.id_membre
JOIN exemplaire E 
ON E.id = P.exemplaire
JOIN RESSOURCES R
ON R.code=E.ressource
WHERE P.etat ='waiting'

SELECT card
FROM ADHERENTS A JOIN PRET P
ON A.id_membre = P.adherent
WHERE P.etat='waiting'
GROUP BY A.card


----Selection de l'information de chaque livre
SELECT Exemplaire.id, etat, titre, nom, prenom
FROM Livres JOIN Auteurs ON Auteurs.code_ressource = Livres.code
JOIN Contributeurs ON Auteurs.id_contributeur = Contributeurs.id
JOIN Exemplaire ON Exemplaire.ressource = Livres.code;

SELECT  code,titre,synopsis,nom, prenom, FilmContributeurs.role
FROM Films JOIN FilmContributeurs ON FilmContributeurs.code_ressource = Films.code
JOIN Contributeurs ON FilmContributeurs.id_contributeur = Contributeurs.id;


SELECT code,titre,duree,nom, prenom, MusiqueContributeurs.role
FROM Musiques JOIN MusiqueContributeurs ON MusiqueContributeurs.code_ressource = Musiques.code
JOIN Contributeurs ON MusiqueContributeurs.id_contributeur = Contributeurs.id;


-----Nombre d'oeuvre de chaque Contributeurs

SELECT COUNT(Ressources.code) AS nombre_oeuvres, nom, prenom
FROM Ressources 
JOIN FilmContributeurs ON Ressources.code = FilmContributeurs.code_ressource
JOIN Contributeurs ON FilmContributeurs.id_contributeur = Contributeurs.id
GROUP BY Contributeurs.id

UNION 

SELECT COUNT(Ressources.code) AS nombre_oeuvres, nom, prenom
FROM Ressources 
JOIN MusiqueContributeurs ON Ressources.code = MusiqueContributeurs.code_ressource
JOIN Contributeurs ON MusiqueContributeurs.id_contributeur = Contributeurs.id
GROUP BY Contributeurs.id

UNION 

SELECT COUNT(Ressources.code) AS nombre_oeuvres, nom, prenom
FROM Ressources 
JOIN Auteurs ON Ressources.code = Auteurs.code_ressource
JOIN Contributeurs ON Auteurs.id_contributeur = Contributeurs.id
GROUP BY Contributeurs.id;


----Le nombre de contributeur pour chaque oeuvre

SELECT Count(id_contributeur), titre
FROM Auteurs 
JOIN Livres ON Auteurs.code_ressource = Livres.code_ressource
GROUP BY (titre)

UNION

SELECT Count(id_contributeur), titre
FROM FilmContributeurs
JOIN Films ON FilmContributeurs.code_ressource = Films.code_ressource
GROUP BY (titre)

UNION

SELECT Count(id_contributeur), titre
FROM MusiqueContributeurs
JOIN Musiques ON MusiqueContributeurs.code_ressource = Musiques.code_ressource
GROUP BY (titre);

---Le nombre d'exemplaire pour chaque oeuvre

SELECT Count(Exemplaire.id), titre
FROM Exemplaire
JOIN Livres ON Exemplaire.ressource = Livres.code_ressource
GROUP BY (titre)

UNION

SELECT Count(Exemplaire.id), titre
FROM Exemplaire
JOIN Films ON Exemplaire.ressource = Films.code_ressource
GROUP BY (titre)

UNION

SELECT Count(Exemplaire.id), titre
FROM Exemplaire
JOIN Musiques ON Exemplaire.ressource = Musiques.code_ressource
GROUP BY (titre);

--afficher les détails de tous les prets 
select id_pret,id,nom,prenom,date_naissance,email,etat
from Pret
inner join Membres
on Membres.id = Pret.adherent

--afficher nom et prenom pour les membres qui fait plus que un prêt en meme temps
select nom , prenom
from Membres
inner join Pret
on Membres.id = Pret.adherent
where etat = 'en cours'
group by adherent
having count(adherent) > 1;

--seulement afficher id de pret qui fait plus que un prêt en meme temps(on peut traiter comme supprimer après)
select id_pret
from Pret 
where etat = 'en cours' 
group by adherent
having count(adherent) > 1;


--afficher nom et prenom pour les membres qui fait plus que un retard(qui va etre dans le blacklist)
select nom , prenom
from Membres
inner join Pret
on Pret.adherent = Membres.id
inner join Retard
on Retard.id_pret = Pret.id_pret
group by adherent
having count(adherent) > 1; 

--seulement afficher id de pret qui fait plus que un retard(qui va etre dans le blacklist)
group by adherent
select id_pret
from Retard
inner join Pret
on Pret.id_pret = Retard.id_pret      
having count(adherent) > 1;

--afficher nom et prenom pour les membres qui ont plus que une Deterioration
select nom , prenom
from Membres
inner join Pret
on Pret.adherent = Membres.id
inner join Deterioration 
on Deterioration.id_pret = Pret.id_pret
group by adherent
having count(adherent) > 1; 

--seulement afficher id de prêt qui fait plus que un retard(qui va etre dans le blacklist)
select id_pret
from Deterioration
inner join Pret
on Pret.id_pret = Deterioration.id_pret      
group by adherent
having count(adherent) > 1;        

--afficher tous les personnes avec la Deterioration comme "en attente"
select nom,prenom
from Membres
inner join Pret
on Pret.adherent = Membres.id
inner join Deterioration 
on Deterioration.id_pret = Pret.id_pret
where Deterioration.etat_remboursement = 'en attente'; 

--afficher id de prêt qui emprunte le document n'est pas dans le bon état 
select id_pret 
from Pret
inner join Exemplaire
on Exemplaire.id = Pret.exemplaire
where etat = 'abime' or etat = 'tres_abime';


